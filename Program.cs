﻿using System;

namespace Seminararbeit
{
    class Program
    {
        int counter = 0;
        int succes = 0;
        double p;
        int a;
        int m;
        double probR;

        Random random = new Random();

        static void Main(string[] args)
        {
            new Program();
        }

        Program() 
        {
            Console.WriteLine("Wie hoch soll die Gewinnwahrscheinlichkeit für eine Runde sein?");
            p = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Wie hoch soll das Startkapital sein?");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wie hoch soll das Gesamtkapital sein?");
            m = Convert.ToInt32(Console.ReadLine());
            probR = ruin();
            Run(Int32.MaxValue);
            Console.WriteLine("Zum Beenden eine beliebige Taste drücken.");
            Console.ReadLine();
        }

        private double ruin()
        {
            if(p == 0.5)
            {
                int b = m - a;
                double ret = (double) b / m;
                return ret;
            }
            else
            {
                double q = 1 - p;
                double u = q / p;
                double uha = 1;
                double uhm = 1;

                for (int i = 0; i < a; i++)
                {
                    uha = uha * u;
                }

                for (int i = 0; i < m; i++)
                {
                    uhm = uhm * u;
                }

                return (uha - uhm) / (1 - uhm);
            }
        }

        private void Run(int rounds)
        {
            while (counter < rounds)
            {
                if (Game(a))
                    succes++;

                counter++;
                Console.WriteLine(counter);
            }

            int lost = counter - succes;

            double prob = (double) lost / counter;
            Console.WriteLine(succes + " Durchläufe von " + counter + " Durchläufen waren erfolgreich! Daraus ergibt sich eine Ruinquote von: " + prob +" Die Berechnete Runwahrcheinlichkeit liegt bei: " +probR);
        }

        private bool Game(int a) 
        {
            a = this.a;

            while (true)
            {
                if (won())
                    a++;
                else
                    a--;

                //Console.WriteLine(a);

                if (a <= 0)
                    return false;
                else if (a >= m)
                    return true;
            }
        }

        private bool won()
        {
            if (random.NextDouble() < p)
            {
                //Console.WriteLine("Runde gewonnen! " + counter);
                return true;
            }
            else
            {
                //Console.WriteLine("Runde verloren! " + counter);
                return false;
            }
        }
    }
}
